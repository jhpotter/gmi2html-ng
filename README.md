# gmi2html-ng

Tired of maintaining two separate websites for Gemini and HTML? With this script
you can simply maintain a Gemini site and automatically convert it to HTML.

## Installation

If you have the [Nix package manager](https://nixos.org), you can install this
program into your local environment by cloning this repository and running:

```
$ nix-env -f default.nix -i
```

Otherwise, you can use Make:

```
$ make install
```

This installs by default to `~/.local/bin`, but an alternative installation path
can be specified with the `PREFIX` environment variable.

## Usage

To convert a directory of Gemini files to HTML:

```
$ gmi2htmldir ${in} ${out} \
    title=${title} \
    css=${css} \
    original=${original}
```

Where:

* `${in}` is the directory of Gemini files
* `${out}` is the directory of HTML files
* `${title}` is the website title (used in the `<title>` tag)
* `${css}` is the path to the CSS stylesheet (`<link href="${css}">`)
* `${original}` is the URL of the Gemini site, included in the footer of the
  generated HTML

Detailed usage instructions are inside the script sources.
