#!/bin/sh
#
# gmi2htmldir -- POSIX shell script to convert a folder of Gemini files to HTML
# files
#
# Copyright (c) 2021-2022 Jeremy Potter (jhpotter)
# Copyright (c) 2022 Rodrigo S. Canibano (dracometallium)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# usage:
#   $ gmi2htmldir <in> <out> title=<title> css=<css> original=<original>
#
# parameters:
#   <in>: the folder of Gemini files to be converted
#   <out>: the folder in which to put the generated HTML files
#   <title>, <css>, <original>: parameters passed to gmi2html.awk; see
#   gmi2html.awk
#

in="$1"
out="$2"
title="$3"
css="$4"
original="$5"

# Loop through all the files in the input directory
for f in $(find "${in}" -type "f" -printf "%P\n"); do

  # If a file is not a Gemini file, just copy it to the
  # output directory
  if [[ "${f}" != *.gmi ]]; then
    echo "copying: ${in}/${f} -> ${out}/${f}"
    cp "${in}/${f}" "${out}/${f}"
    continue
  fi

  # Replace .gmi extension with .html
  HTML_FILENAME="${out}/${f%.gmi}.html"

  # Check if the source file has been changed; if not, just
  # skip it
  if [ ! -e "${HTML_FILENAME}" ] || [ "$f" -nt "${HTML_FILENAME}" ]; then
    echo "processing: ${in}/${f} -> ${HTML_FILENAME}"

    # Create all the output subdirectories if they do not
    # exist already
    mkdir -p $(dirname -- "${HTML_FILENAME}")

    # Run the gmi2html AWK script, passing in the parameters
    awk -f $(which gmi2html) \
      -v "${title}" -v "${css}" -v "${original}" \
      < "${in}/${f}" \
      > "${HTML_FILENAME}"
  else
    echo "skipping: ${f} -> ${HTML_FILENAME}"
  fi
done
