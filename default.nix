with import <nixpkgs> {};

stdenvNoCC.mkDerivation
{
    name = "gmi2html-ng";

    phases =
    [
        "installPhase"
    ];

    src = ./.;

    installPhase =
    ''
    mkdir -p $out/bin
    cp $src/gmi2html.awk $out/bin/gmi2html
    cp $src/gmi2htmldir.sh $out/bin/gmi2htmldir
    '';
}