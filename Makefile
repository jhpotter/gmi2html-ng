PREFIX ?= $(HOME)/.local

install:
	mkdir -p $(PREFIX)/bin
	install gmi2html.awk $(PREFIX)/bin/gmi2html
	install gmi2htmldir.sh $(PREFIX)/bin/gmi2htmldir
